const PI_180 = Math.PI/180;
class Player{
    constructor(scene){
        this.scene = scene;
        //this.mesh =  this.scene.getMeshByName("pato");

        //this.mesh.checkCollisions = true;
        //this.mesh.ellipsoid = new BABYLON.Vector3(0.25, 0.25, 0.25);
        //this.mesh.ellipsoidOffset = new BABYLON.Vector3(0, 90, 0);



        this.state = "Idle";

        this.gravity = new BABYLON.Vector3();
        this.velocity = new BABYLON.Vector3();
        this.vr = 0;
        this.rotation = -Math.PI/2;
        this.speed = 0;
        this.jump_heigth = 6;
        this.canJump = true;
        this.inFloor = false;
    }

    update_player(elapsed){
        this.rotation += this.vr * PI_180;
        this.velocity.x = -Math.cos(this.rotation) * this.speed * elapsed;
        this.velocity.y += this.scene.gravity.y * elapsed;
        this.velocity.z =  Math.sin(this.rotation) * this.speed * elapsed;

        this.mesh.rotation.y = this.rotation +Math.PI/2;
        // this.cameraTarget.rotation.y = this.mesh.rotation.y;

        //this.mesh.moveWithCollisions(this.velocity);
        this.mesh.moveWithCollisions(new BABYLON.Vector3(0,-9.8,0));

        if (this.inFloor) {
            this.velocity.y = Math.max(this.scene.gravity.y, this.velocity.y);
        }
        this.velocity.y = Math.max(this.scene.gravity.y, this.velocity.y);
        this.inFloor = false;
    }

    process_input(keys){
        this.vr = 0;

        if (keys["ArrowUp"]) {
            this.mesh.moveWithCollisions(new BABYLON.Vector3(20.5,0,0));
        }

        if (keys["ArrowLeft"]) {
            this.mesh.rotation.y =  -Math.PI / 90;
        }

        if (keys["ArrowRight"]) {
            this.mesh.rotation.y =  Math.PI / 90;
        }
        if (keys["ArrowDown"]) {
            this.mesh.moveWithCollisions(new BABYLON.Vector3(-20.5,0,0));
        }

        if (!keys["z"] && !keys["Z"]) {
            this.canJump = true;
        }
        if (this.canJump && this.inFloor && (keys["z"] || keys["Z"])) {
            this.velocity.y = this.jump_heigth;
            this.canJump = false;
        }
    }

}