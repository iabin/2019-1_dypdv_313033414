
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

class Enemy1 {

    constructor(scene, id, y = 200) {
        var x = getRndInteger(-1000,1000);
        var z = getRndInteger(-1000,1000);
        this.scene = scene;
        this.mesh = BABYLON.MeshBuilder.CreateSphere("sphere", {diameter: 50, diameterX: 50}, this.scene);
        //this.mesh = this.scene.getMeshByName("pato").clone("My_" + id);
        console.log(this.mesh);
        this.mesh.checkCollisions = true;
        this.mesh.ellipsoid = new BABYLON.Vector3(0.911918 / 2, 1.15347 / 2, 1.02883 / 2);
        this.mesh.ellipsoidOffset = new BABYLON.Vector3(0, 1.15347 / 2, 0);
        this.mesh.position.x = x;
        this.mesh.position.y = y;
        this.mesh.position.z = z;

        // let debug_ellipsoid = BABYLON.MeshBuilder.CreateSphere("tmp_sphere", {diameterX: 0.911918, diameterY: 1.15347, diameterZ: 1.02883}, scene);
        // debug_ellipsoid.position.y = 1.15347/2;
        // debug_ellipsoid.parent = this.mesh;
        // debug_ellipsoid.visibility = 0.25;

        this.state = "Idle";


        this.gravity = new BABYLON.Vector3();
        this.velocity = new BABYLON.Vector3();
        this.vr = 0;
        this.rotation = 0;
        this.speed = 70;
        this.jump_heigth = 0.1;
        this.canJump = true;
        this.inFloor = false;
    }

    randomWalk() {

        if (Math.random() < 0.25) {
            this.rotation = (Math.random() < 0.5) ? Math.PI / 4 : -Math.PI / 4;
        }

        else if (Math.random() < 0.45) {
            this.rotation = (Math.random() < 0.5) ? Math.PI*3 / 3 : -Math.PI*3 / 4;
        }

    }

    update(elapsed) {
        //this.speed += 0.01;
        this.randomWalk();

        this.velocity.x = -Math.cos(this.rotation) * this.speed * elapsed;
        this.velocity.y += this.scene.gravity.y * elapsed;
        this.velocity.z = Math.sin(this.rotation) * this.speed * elapsed;

        this.mesh.rotation.y = this.rotation + Math.PI / 2;

        this.mesh.moveWithCollisions(this.velocity);

        if (this.inFloor) {
            this.velocity.y = Math.max(this.scene.gravity.y, this.velocity.y);
        }

        this.inFloor = false;
    }

}

