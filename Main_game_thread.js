let keys = {};

class Game {

    constructor(scene, engine) {
        this.scene = scene;
        this.engine = engine;
        this.scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
        this.scene.collisionsEnabled = true;
        scene.ambientColor = new BABYLON.Color3(0.75, 0.75, 0.75);

        var canvas = document.getElementById('renderCanvas');

        // Need a free camera for collisions
        var camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(0, 100,-450), scene);
        var music = new BABYLON.Sound("Violons", "musica.mp3", scene, null, { loop: true, autoplay: true });
        camera.attachControl(canvas, true);

        /**
        // Need a free camera for collisions
        var camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(0, 100, 10), scene);
        camera.attachControl(canvas, true);


        //Then apply collisions and gravity to the active camera
        camera.checkCollisions = true;
        camera.applyGravity = true;
        */
        //Set the ellipsoid around the camera (e.g. your player's size)

        camera.checkCollisions = true;
        camera.applyGravity = true;
        camera.ellipsoid = new BABYLON.Vector3(3, 3, 3);



        // https://doc.babylonjs.com/how_to/skybox
        var skybox = BABYLON.MeshBuilder.CreateBox("SkyBox", {size: 4600}, this.scene);
        var skyboxMaterial = new BABYLON.StandardMaterial("SkyBoxMat", this.scene);
        skyboxMaterial.backFaceCulling = false;
        // skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("models/skybox/skybox", this.scene);
        skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://www.babylonjs-playground.com/textures/TropicalSunnyDay", this.scene);
        skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
        skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        skybox.material = skyboxMaterial;


        this.light = new BABYLON.DirectionalLight("directionalLight",
            new BABYLON.Vector3(-1, -2, -1),
            this.scene);
        this.light.position = new BABYLON.Vector3(0, 150, 20);


        let shadowGenerator = new BABYLON.ShadowGenerator(1024, this.light);


        var ground = BABYLON.Mesh.CreateGround("Ground", 4600, 4600, 2, this.scene);
        this.ground =  ground;
        this.ground.material = new BABYLON.StandardMaterial("GroundMat", this.scene);

        this.ground.material.diffuseColor = new BABYLON.Color3(0.3, 0.2, 0);
        this.ground.material.specularColor = new BABYLON.Color3(0, 0, 0);

        this.ground2 = this.scene.getMeshByName("lago");
        this.ground.receiveShadows = true;
        this.ground.position.y = 50;
        this.ground.visibility =0;
        this.ground.checkCollisions = true;
        this.ground2 = this.scene.getMeshByName("lago");
        this.ground2.receiveShadows = true;
        this.ground2.checkCollisions = true;

        camera.onCollide = function (colMesh) {
            if (colMesh.uniqueId === ground.uniqueId) {
                //console.log("Tocando Suelo");
                return;
            }
            //alert("Has muerto");
            window.location.replace("./GameOver.html");
        }


        //let wall1 = BABYLON.MeshBuilder.CreateBox("Wall1", {width: 1000, height: 350, depth: 10}, this.scene);
        //let wall1 = BABYLON.Mesh.CreateGround("Ground", 4600, 4600, 2, this.scene);
        //let wall1 = this.ground2 = this.scene.getMeshByName("pato");
        //wall1.scaling = new BABYLON.Vector3(1000, 1000, 1000);

        //wall1.position = new BABYLON.Vector3(0, -45, -500);
        //wall1.checkCollisions = true;


        this.player = new Player(this.scene);
        this.enemies = [];
        for (var i = 0; i<20;i++){
            var enemy =  new Enemy1(this.scene,i);
            this.enemies[i] = enemy;
        }

        //this.camera.lockedTarget = this.player.cameraTarget;

        // register keyboard input
        this.scene.actionManager = new BABYLON.ActionManager(this.scene);
        this.scene.actionManager.registerAction(
            new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger,
                function (evt) {
                    keys[evt.sourceEvent.key] = true;
                }
            )
        );

        this.scene.actionManager.registerAction(
            new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger,
                function (evt) {
                    delete keys[evt.sourceEvent.key];
                }
            )
        );


        //shadowGenerator.getShadowMap().renderList.push(this.player.mesh);
        //this.camera.lockedTarget = this.player.cameraTarget;

    }

    renderGame() {
        this.scene.render();
    }

    process_input() {
        //this.player.process_input(keys);
    }

    updateState(elapsed) {
        //this.player.update_player(elapsed);

       for (var i = 0;i<this.enemies.length;i++){
           this.enemies[i].update(elapsed);
       }
        //if (this.ground.intersectsMesh(this.player.mesh, false)) {
          //  console.log("Tocando");
            //this.player.inFloor = true;
        //}
    }


}


