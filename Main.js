function rgbToBabylon(r, g, b) {
    return new BABYLON.Color3(r / 255, g / 255, b / 255);
}


window.addEventListener('DOMContentLoaded', function () {
    var canvas = document.getElementById('renderCanvas');
    let engine = new BABYLON.Engine(canvas, true);


    // Create a basic BJS Scene object.
    var scene = new BABYLON.Scene(engine);

    var assetsManager = new BABYLON.AssetsManager(scene);

    var lakemeshTask = assetsManager.addMeshTask("lake", "", "models/", "lake.glb");
    //var duckmeshTask = assetsManager.addMeshTask("duck", "", "models/", "duck.glb");

    lakemeshTask.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i=0; i<loadedMeshes.length; i++) {
            //loadedMeshes[i].checkCollisions = true;
            loadedMeshes[i].scaling = new BABYLON.Vector3(20, 20, 20);
        }
        console.log(loadedMeshes[0].name);
        //loadedMeshes[0].scaling = new BABYLON.Vector3(5000, 5000, 5000);
        loadedMeshes[0].name = "lago";
    };
/**
    duckmeshTask.onSuccess = function (task) {
        let loadedMeshes = task.loadedMeshes;
        for (let i=0; i<loadedMeshes.length; i++) {
            loadedMeshes[i].checkCollisions = false;
            loadedMeshes[i].position.y = 2;

            loadedMeshes[i].scaling = new BABYLON.Vector3(5, 5, 5);
        }
        let duck = task.loadedMeshes[0];
        console.log(duck.name);

        duck.name = "pato";

    };
*/
    assetsManager.onFinish = function (tasks) {
        let game = new Game(scene,engine);

        engine.runRenderLoop(function () {
            game.updateState(engine.getDeltaTime()/1000);
            game.process_input();
            game.renderGame();
        });

    };


    assetsManager.load();




    window.addEventListener('resize', function () {
        engine.resize();
    });


});